package main

import (
	"fmt"
	"github.com/go-redis/redis"
	"net/http"
	"gopkg.in/alecthomas/kingpin.v2"
	"github.com/Shopify/sarama"
	"strings"
)


var httpClient *http.Client

const (
	MaxIdleConnections int = 20
	RequestTimeout     int = 5
)

var (
	kafkaConn = []string{"central-kafka-cluster-1.swiggyperf.in:9092","central-kafka-cluster-2.swiggyperf.in:9092","central-kafka-cluster-3.swiggyperf.in:9092","central-kafka-cluster-4.swiggyperf.in:9092","central-kafka-cluster-5.swiggyperf.in:9092","central-kafka-cluster-6.swiggyperf.in:9092"}
	mytopic = "order_status_update_v2"
	brokerList = kingpin.Flag("brokerList", "List of brokers to connect").Default(kafkaConn...).Strings()
	topic      = kingpin.Flag("topic", "aaaaa").Default("batch_info_event").String()
	maxRetry   = kingpin.Flag("maxRetry", "Retry limit").Default("5").Int()
)

func main() {
	redisdb := redis.NewClient(&redis.Options{
		Addr:     "test-delivery-data.chl2tw.0001.use1.cache.amazonaws.com:6379",
		Password: "", // no password set
		DB:       8, // use default DB
	})

	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = *maxRetry
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		fmt.Printf("aaaaaaaa================")
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	pong, err := redisdb.Ping().Result()
	fmt.Println("redis connection", pong, err)


	for {
		//fmt.Println("redis lpop",redisdb.LPop("tst_key"))
		order := redisdb.LPop("tst_batch").String()
		//fmt.Println(order)
		if order != "" {
			orders := strings.Split(order, ":")
			if len(orders) >= 5 {
				//fmt.Println(orders[2] + orders[3] + orders[4])
				batch_delivered := "{ \"batch_id\": ${batch_id}, \"orders\": [ \"${order_ids}\" ],\"event\":\"batch_delivered\" }"

				batch_msg := strings.NewReplacer("${batch_id}", orders[2], "${order_ids}", orders[3] + "\",\"" + orders[4])
				batch_delivered = batch_msg.Replace(batch_delivered)

				bmsg := &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(batch_delivered),
				}
				producer.SendMessage(bmsg)
				//fmt.Println(bmsg)
			}
		}
	}
}
