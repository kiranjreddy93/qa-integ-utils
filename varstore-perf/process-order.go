package main

import (
	//      "log"
	"fmt"
	"time"
	"github.com/Shopify/sarama"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
	"github.com/go-redis/redis"
	"strconv"
	"strings"
)


var (
	kafkaConn = []string{"central-kafka-cluster-1.swiggyperf.in:9092","central-kafka-cluster-2.swiggyperf.in:9092","central-kafka-cluster-3.swiggyperf.in:9092","central-kafka-cluster-4.swiggyperf.in:9092","central-kafka-cluster-5.swiggyperf.in:9092","central-kafka-cluster-6.swiggyperf.in:9092"}
	mytopic = "order_status_update_v2"
	brokerList = kingpin.Flag("brokerList", "List of brokers to connect").Default(kafkaConn...).Strings()
	topic      = kingpin.Flag("topic", "aaaaa").Default("order_status_update_v2").String()
	maxRetry   = kingpin.Flag("maxRetry", "Retry limit").Default("5").Int()
)

func main() {

	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = *maxRetry
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		fmt.Printf("aaaaaaaa================")
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()
	redisdb := redis.NewClient(&redis.Options{
		Addr:     "test-delivery-data.chl2tw.0001.use1.cache.amazonaws.com:6379",
		Password: "", // no password set
		DB:       8,  // use default DB
	})

	pong, err := redisdb.Ping().Result()
	fmt.Println("redis connection",pong, err)

	fmt.Println("redis lpop",redisdb.LPop("tst_key"))
	start := time.Now()
	for {
		for i :=1;i<=6000;i++ {
			order_id := redisdb.LPop("tst_process").Val()
			//fmt.Println("order id is:" ,order_id)
			if order_id != "" {
				currentTime := time.Now()
				currentTime = currentTime.Add(5*time.Minute)
				assigned := "{\"status\":\"assigned\",\"source\":\"AUTO_ASSIGN\",\"order_id\":\"${order_id}\",\"batch_count\":1,\"deDetails\":{\"mobileNumber\":\"7406190991\",\"name\":\"Test - Ravi\",\"altMobile\":\"7406190991\",\"nick\":\"Test - Ravi\",\"id\":1026,\"imgUrl\":\"https:de-docs.s3.amazonaws.comphotographs22904.jpg\"},\"partner_name\":\"Swiggy\",\"deliveryboy_id\":1026,\"time\":\"${time}\",\"batch_id\":18271772,\"geo_details\":{\"city_id\":\"${city_id}\",\"zone_id\":\"${zone_id}\"}}"
				//      fmt.Println(assigned)
				a1 := strings.NewReplacer("${order_id}", order_id,"${city_id}", strconv.Itoa(i % 8), "${zone_id}", strconv.Itoa(i % 2398), "${time}", currentTime.Format("2006-01-02 15:04:05"))
				assigned = a1.Replace(assigned)

				currentTime = currentTime.Add(5*time.Minute)
				order_start := "{\"status\":\"order_start\",\"source\":\"AUTO_ASSIGN\",\"order_id\":\"${order_id}\",\"batch_count\":1,\"deDetails\":{\"mobileNumber\":\"7406190991\",\"name\":\"Test - Ravi\",\"altMobile\":\"7406190991\",\"nick\":\"Test - Ravi\",\"id\":1026,\"imgUrl\":\"https:de-docs.s3.amazonaws.comphotographs22904.jpg\"},\"partner_name\":\"Swiggy\",\"deliveryboy_id\":1026,\"time\":\"${time}\",\"batch_id\":18271772,\"geo_details\":{\"city_id\":\"${city_id}\",\"zone_id\":\"${zone_id}\"}}"
				b1 := strings.NewReplacer("${order_id}", order_id,"${city_id}", strconv.Itoa(i % 8), "${zone_id}", strconv.Itoa(i % 2398), "${time}", currentTime.Format("2006-01-02 15:04:05"))
				order_start = b1.Replace(order_start)
				//order_start = strings.Replace(order_start, "${order_id}", order_id, -1)

				currentTime = currentTime.Add(5*time.Minute)
				confirmed := "{\"status\":\"confirmed\",\"source\":\"AUTO_ASSIGN\",\"order_id\":\"${order_id}\",\"batch_count\":1,\"deDetails\":{\"mobileNumber\":\"7406190991\",\"name\":\"Test - Ravi\",\"altMobile\":\"7406190991\",\"nick\":\"Test - Ravi\",\"id\":1026,\"imgUrl\":\"https:de-docs.s3.amazonaws.comphotographs22904.jpg\"},\"partner_name\":\"Swiggy\",\"deliveryboy_id\":1026,\"time\":\"${time}\",\"batch_id\":18271772,\"geo_details\":{\"city_id\":\"${city_id}\",\"zone_id\":\"${zone_id}\"}}"
				c1 := strings.NewReplacer("${order_id}", order_id,"${city_id}", strconv.Itoa(i % 8), "${zone_id}", strconv.Itoa(i % 2398), "${time}", currentTime.Format("2006-01-02 15:04:05"))
				confirmed = c1.Replace(confirmed)
				//confirmed = strings.Replace(confirmed,  "${order_id}", order_id, -1)

				currentTime = currentTime.Add(5*time.Minute)
				arrived := "{\"status\":\"arrived\",\"source\":\"AUTO_ASSIGN\",\"order_id\":\"${order_id}\",\"batch_count\":1,\"deDetails\":{\"mobileNumber\":\"7406190991\",\"name\":\"Test - Ravi\",\"altMobile\":\"7406190991\",\"nick\":\"Test - Ravi\",\"id\":1026,\"imgUrl\":\"https:de-docs.s3.amazonaws.comphotographs22904.jpg\"},\"partner_name\":\"Swiggy\",\"deliveryboy_id\":1026,\"time\":\"${time}\",\"batch_id\":18271772,\"geo_details\":{\"city_id\":\"${city_id}\",\"zone_id\":\"${zone_id}\"}}"
				d1 := strings.NewReplacer("${order_id}", order_id,"${city_id}", strconv.Itoa(i % 8), "${zone_id}", strconv.Itoa(i % 2398), "${time}", currentTime.Format("2006-01-02 15:04:05"))
				arrived = d1.Replace(arrived)
				//arrived = strings.Replace(arrived,  "${order_id}", order_id, -1)

				currentTime = currentTime.Add(5*time.Minute)
				pickedup := "{\"status\":\"pickedup\",\"source\":\"AUTO_ASSIGN\",\"order_id\":\"${order_id}\",\"batch_count\":1,\"deDetails\":{\"mobileNumber\":\"7406190991\",\"name\":\"Test - Ravi\",\"altMobile\":\"7406190991\",\"nick\":\"Test - Ravi\",\"id\":1026,\"imgUrl\":\"https:de-docs.s3.amazonaws.comphotographs22904.jpg\"},\"partner_name\":\"Swiggy\",\"deliveryboy_id\":1026,\"time\":\"${time}\",\"batch_id\":18271772,\"geo_details\":{\"city_id\":\"${city_id}\",\"zone_id\":\"${zone_id}\"}}"
				e1 := strings.NewReplacer("${order_id}", order_id,"${city_id}", strconv.Itoa(i % 8), "${zone_id}", strconv.Itoa(i % 2398), "${time}", currentTime.Format("2006-01-02 15:04:05"))
				pickedup = e1.Replace(pickedup)
				//pickedup = strings.Replace(pickedup,  "${order_id}", order_id, -1)

				currentTime = currentTime.Add(5*time.Minute)
				reached := "{\"status\":\"reached\",\"source\":\"AUTO_ASSIGN\",\"order_id\":\"${order_id}\",\"batch_count\":1,\"deDetails\":{\"mobileNumber\":\"7406190991\",\"name\":\"Test - Ravi\",\"altMobile\":\"7406190991\",\"nick\":\"Test - Ravi\",\"id\":1026,\"imgUrl\":\"https:de-docs.s3.amazonaws.comphotographs22904.jpg\"},\"partner_name\":\"Swiggy\",\"deliveryboy_id\":1026,\"time\":\"${time}\",\"batch_id\":18271772,\"geo_details\":{\"city_id\":\"${city_id}\",\"zone_id\":\"${zone_id}\"}}"
				f1 := strings.NewReplacer("${order_id}", order_id,"${city_id}", strconv.Itoa(i % 8), "${zone_id}", strconv.Itoa(i % 2398), "${time}", currentTime.Format("2006-01-02 15:04:05"))
				reached = f1.Replace(reached)
				//reached = strings.Replace(reached,  "${order_id}", order_id, -1)

				currentTime = currentTime.Add(5*time.Minute)
				delivered := "{\"status\":\"delivered\",\"source\":\"AUTO_ASSIGN\",\"order_id\":\"${order_id}\",\"batch_count\":1,\"deDetails\":{\"mobileNumber\":\"7406190991\",\"name\":\"Test - Ravi\",\"altMobile\":\"7406190991\",\"nick\":\"Test - Ravi\",\"id\":1026,\"imgUrl\":\"https:de-docs.s3.amazonaws.comphotographs22904.jpg\"},\"partner_name\":\"Swiggy\",\"deliveryboy_id\":1026,\"time\":\"${time}\",\"batch_id\":18271772,\"geo_details\":{\"city_id\":\"${city_id}\",\"zone_id\":\"${zone_id}\"}}"
				g1 := strings.NewReplacer("${order_id}", order_id,"${city_id}", strconv.Itoa(i % 8), "${zone_id}", strconv.Itoa(i % 2398), "${time}", currentTime.Format("2006-01-02 15:04:05"))
				delivered = g1.Replace(delivered)
				//delivered = strings.Replace(delivered,  "${order_id}", order_id, -1)

				msg := &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(assigned),
				}
				//partition, offset, err := producer.SendMessage(msg)
				producer.SendMessage(msg)
				start = time.Now()
				//elapsed := time.Since(start)
				//fmt.Println("elapsed time is %s", elapsed)
				//time.Sleep(1*time.Microsecond)

				msgc := &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(confirmed),
				}
				producer.SendMessage(msgc)
				//time.Sleep(1*time.Microsecond)

				msgo := &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(order_start),
				}
				producer.SendMessage(msgo)
				//time.Sleep(1*time.Microsecond)

				msga := &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(arrived),
				}
				producer.SendMessage(msga)
				//time.Sleep(1*time.Microsecond)

				msgp := &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(pickedup),
				}
				//producer.SendMessage(msgp)
				//time.Sleep(1*time.Microsecond)

				msgp = &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(reached),
				}
				//producer.SendMessage(msgr)
				//time.Sleep(1*time.Microsecond)

				msgp = &sarama.ProducerMessage{
					Topic: *topic,
					Value: sarama.StringEncoder(delivered),
				}
				//producer.SendMessage(msgd)
				fmt.Println(msgd)
				//time.Sleep(1*time.Microsecond)

				if err != nil {
					fmt.Printf("some error occured")
					//panic(err)
				}
				//fmt.Println(msgp)
				//fmt.Println(msgr)
				//elapsed := time.Since(start)
				//fmt.Println("elapsed time is %s", elapsed)
				//fmt.Println("assigned valie is",assigned)
				//fmt.Printf( " :: Message is stored in topic(%s)/partition(%d)/offset(%d)\n", *topic, partition, offset)
				//time.Sleep(1 * time.Microsecond)
			}
		}
	}
	elapsed := time.Since(start)
	fmt.Printf("elapsed time is %s", elapsed)
}
