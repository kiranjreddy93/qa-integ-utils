package main

import (
	"fmt"
	"time"
	"github.com/go-redis/redis"
	"net/http"
	"gopkg.in/alecthomas/kingpin.v2"
	"github.com/Shopify/sarama"
	"strings"
)


var httpClient *http.Client

const (
	MaxIdleConnections int = 20
	RequestTimeout     int = 5
)

var (
	kafkaConn = []string{"central-kafka-cluster-1.swiggyperf.in:9092","central-kafka-cluster-2.swiggyperf.in:9092","central-kafka-cluster-3.swiggyperf.in:9092","central-kafka-cluster-4.swiggyperf.in:9092","central-kafka-cluster-5.swiggyperf.in:9092","central-kafka-cluster-6.swiggyperf.in:9092"}
	mytopic = "order_status_update_v2"
	brokerList = kingpin.Flag("brokerList", "List of brokers to connect").Default(kafkaConn...).Strings()
	topic      = kingpin.Flag("topic", "aaaaa").Default("order_status_update_v2").String()
	maxRetry   = kingpin.Flag("maxRetry", "Retry limit").Default("5").Int()
)

func init() {
	httpClient = createHTTPClient()
}

// createHTTPClient for connection re-use
func createHTTPClient() *http.Client {
	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: MaxIdleConnections,
		},
		Timeout: time.Duration(RequestTimeout) * time.Second,
	}

	return client
}
func main() {
	redisdb := redis.NewClient(&redis.Options{
		Addr:     "test-delivery-data.chl2tw.0001.use1.cache.amazonaws.com:6379",
		Password: "", // no password set
		DB:       8, // use default DB
	})

	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = *maxRetry
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		fmt.Printf("aaaaaaaa================")
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	pong, err := redisdb.Ping().Result()
	fmt.Println("redis connection", pong, err)


	for {

		//fmt.Println("redis lpop",redisdb.LPop("tst_key"))
		order := redisdb.LPop("tst_pick_up").String()
		if order != "" {
			orders := strings.Split(order, ":")
			//fmt.Println(orders[0] +" "+ orders[1] +" "+ orders[2]+" "+orders[3])
			picked_up := "{ \"batch_id\": ${batch_id}, \"order_id\": \"${order_id}\", \"restaurant_id\": 539, \"deDetails\": { \"id\": 599836 }, \"status\": \"pickedup\", \"time\": \"2019-08-21 18:20:54\", \"partner_name\": \"Swiggy\", \"geo_details\": { \"zone_id\": \"12\", \"city_id\": \"1\" } }"
			delivered := "{ \"batch_id\": ${batch_id}, \"order_id\": \"${order_id}\", \"restaurant_id\": 539, \"deDetails\": { \"id\": 599836 }, \"status\": \"delivered\", \"time\": \"2019-08-21 18:20:54\", \"partner_name\": \"Swiggy\", \"geo_details\": { \"zone_id\": \"12\", \"city_id\": \"1\" } }"
			batch_delivered := "{ \"batch_id\": ${batch_id}, \"orders\": [ \"${order_ids}\" ] }"

			msg := strings.NewReplacer("${batch_id}", orders[1], "${order_id}", orders[2])
			picked_up = msg.Replace(picked_up)

			msgc := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(picked_up),
			}
			producer.SendMessage(msgc)
			//fmt.Println(msgc)

			msg = strings.NewReplacer("${batch_id}", orders[1], "${order_id}", orders[3])
			//picked_up = msg.Replace(picked_up)

			msgc = &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(picked_up),
			}
			producer.SendMessage(msgc)
			//fmt.Println(msgc)

			msgd := strings.NewReplacer("${batch_id}", orders[1], "${order_id}", orders[2])
			delivered = msgd.Replace(delivered)

			msgc = &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(delivered),
			}
			producer.SendMessage(msgc)
			//fmt.Println(msgc)

			msgd = strings.NewReplacer("${batch_id}", orders[1], "${order_id}", orders[3])
			delivered = msgd.Replace(delivered)

			msgc = &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(delivered),
			}
			producer.SendMessage(msgc)
			//fmt.Println(msgc)

			batch_msg := strings.NewReplacer("${batch_id}", orders[1], "${order_ids}", orders[2] +","+orders[3])
			batch_delivered = batch_msg.Replace(batch_delivered)

			bmsg := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(batch_delivered),
			}
			producer.SendMessage(bmsg)
			//fmt.Println(bmsg)
			redisdb.RPush("tst_batch", order)
		}
	}
}
