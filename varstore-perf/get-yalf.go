package main

import (
	"time"
	"net/http"
	"fmt"
	"encoding/base64"
	//"strings"
	"strconv"
)

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func main() {
	for {
		start := time.Now()
		for i := 1; i <= 2398; i++ {
			time.Sleep(30 * time.Millisecond)
			client := &http.Client{}
			url := "http://varstore.swiggyperf.in/v2/function/yalf/"+strconv.Itoa(i)+"?window=2&batching=1.1&o2dModelType=A"
			req, err := http.NewRequest("GET", url, nil)
			//req.Header.Add("Authorization","Basic " + basicAuth("perf_user","Swiggy@123"))
			resp, err := client.Do(req)
			if err != nil {
				fmt.Println("response is", resp, err)
			}
			//fmt.Println("response is", resp, err)
			resp.Body.Close()
		}
		//time.Sleep(1 * time.Minute)
		elapsed := time.Since(start)
		fmt.Printf("elapsed time is %s", elapsed)
	}
}