package main

import (
	"fmt"
	"time"
	"github.com/go-redis/redis"
	"net/http"
	"bytes"
	"io/ioutil"
	"log"
	"encoding/json"
	"database/sql"
	"gopkg.in/alecthomas/kingpin.v2"
	"github.com/Shopify/sarama"
	"strings"
	"strconv"
	_ "github.com/go-sql-driver/mysql"
)


var httpClient *http.Client

const (
	MaxIdleConnections int = 20
	RequestTimeout     int = 5
)

var (
	kafkaConn = []string{"central-kafka-cluster-1.swiggyperf.in:9092","central-kafka-cluster-2.swiggyperf.in:9092","central-kafka-cluster-3.swiggyperf.in:9092","central-kafka-cluster-4.swiggyperf.in:9092","central-kafka-cluster-5.swiggyperf.in:9092","central-kafka-cluster-6.swiggyperf.in:9092"}
	mytopic = "order_status_update_v2"
	brokerList = kingpin.Flag("brokerList", "List of brokers to connect").Default(kafkaConn...).Strings()
	topic      = kingpin.Flag("topic", "aaaaa").Default("batch_task_sequence").String()
	maxRetry   = kingpin.Flag("maxRetry", "Retry limit").Default("5").Int()
)

var db *sql.DB = nil

func GetDB() (*sql.DB, error) {
	if db == nil {
		conn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=require",
			"perf-delivery-new-db.chtvr7orung7.us-east-1.rds.amazonaws.com:3306", "delivery", "dragonstone#123", "swiggy")
		log.Println("Creating a new connection: %v", conn)

		d, err := sql.Open("mysql", "delivery:dragonstone#123@tcp(perf-delivery-new-db.chtvr7orung7.us-east-1.rds.amazonaws.com:3306)/swiggy")
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		db = d
	}

	return db, nil
}

func init() {
	httpClient = createHTTPClient()
}

// createHTTPClient for connection re-use
func createHTTPClient() *http.Client {
	client := &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: MaxIdleConnections,
		},
		Timeout: time.Duration(RequestTimeout) * time.Second,
	}

	return client
}
func main() {
	redisdb := redis.NewClient(&redis.Options{
		Addr:     "test-delivery-data.chl2tw.0001.use1.cache.amazonaws.com:6379",
		Password: "", // no password set
		DB:       8, // use default DB
	})

	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = *maxRetry
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		fmt.Printf("aaaaaaaa================")
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()

	pong, err := redisdb.Ping().Result()
	fmt.Println("redis connection", pong, err)


	for {

		order_id1 := redisdb.LPop("tst_key").Val()
		order_id2 := redisdb.LPop("tst_key").Val()

		if order_id1 != "" && order_id2 != "" {

			//fmt.Println(order_id1, order_id2)
			var batch_id_1 int64
			var batch_id_2 int64
			db, _ := GetDB()
			err = db.QueryRow("select batch_id from trips where order_id='"+order_id1+"'").Scan(&batch_id_1)
			err = db.QueryRow("select batch_id from trips where order_id='"+order_id2+"'").Scan(&batch_id_2)

			//fmt.Println(batch_id_1, batch_id_2)

			endPoint := "http://delivery-service.swiggyperf.in/api/v1/batch/merge"

			//child := "[" + strconv.FormatInt(batch_id_1, 10) + "," + strconv.FormatInt(batch_id_2, 10) + "]"
			var child[] int64
			child = append(child, batch_id_1)
			child = append(child, batch_id_2)
			values := map[string]interface{}{"parent_batch_id": batch_id_1, "childs": child}
			jsonValue, _ := json.Marshal(values)

			req, err := http.NewRequest("POST", endPoint, bytes.NewBuffer([]byte(jsonValue)))
			if err != nil {
				log.Fatalf("Error Occured. %+v", err)
			}
			req.Header.Set("Content-Type", "application/json")
			req.SetBasicAuth("GGYSWI","2015SW!GGY")
			//log.Println(req.Body)
			response, err := httpClient.Do(req)
			if err != nil && response == nil {
				log.Fatalf("Error sending request to API endpoint. %+v", err)
			}

			// Close the connection to reuse it
			defer response.Body.Close()

			// Let's check if the work actually is done
			// We have seen inconsistencies even when we get 200 OK response
			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				log.Fatalf("Couldn't parse response body. %+v", err)
			}

			log.Println("Response Body:", string(body))

			batch_event := "{ \"batch_id\": ${batch_id}, \"event\": \"ASSIGNED\", \"city_id\": 1, \"service_line\": \"FOOD\", \"orders\": [ \"${order_id_1}\", \"${order_id_2}\" ], \"task_sequence\": [ { \"type\": \"pickup\", \"order_id\": \"${order_id_1}\", \"lat_long\": \"12.933491,77.614285\", \"task_leg_time\": 0 }, { \"type\": \"pickup\", \"order_id\": \"${order_id_2}\", \"lat_long\": \"12.933491,77.614285\", \"task_leg_time\": 0 }, { \"type\": \"deliver\", \"order_id\": \"${order_id_1}\", \"lat_long\": \"12.932919,77.602866\", \"task_leg_time\": 0 }, { \"type\": \"deliver\", \"order_id\": \"${order_id_2}\", \"lat_long\": \"12.932919,77.602866\", \"task_leg_time\": 0 } ], \"publish_time\": \"2019-08-21 14:33:12\" }";
			msg := strings.NewReplacer("${order_id_1}", order_id1, "${order_id_2}", order_id2, "${batch_id}", strconv.FormatInt(batch_id_1,10))
			batch_event = msg.Replace(batch_event)

			msgc := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(batch_event),
			}
			producer.SendMessage(msgc)
			//fmt.Println(msgc)
			redisdb.RPush("tst_pick_up", strconv.FormatInt(batch_id_1, 10) + ":" + order_id1 + ":" + order_id2)
			redisdb.RPush("tst_process", order_id1)
			redisdb.RPush("tst_process", order_id2)
		}
	}
}
