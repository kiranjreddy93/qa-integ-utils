package crude_scripts

//update build number from Prod to Perf

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"io/ioutil"
	"log"
	"net/http"
	"bytes"
	"github.com/golang/glog"
	"sync"
	"strings"
	"time"
)

type Service struct {
	serviceName string
}

type Buildstruct []struct {
	LockIndex   int    `json:"LockIndex"`
	Key         string `json:"Key"`
	Flags       int    `json:"Flags"`
	Value       string `json:"Value"`
	CreateIndex int    `json:"CreateIndex"`
	ModifyIndex int    `json:"ModifyIndex"`
}



func getBuildNumberFromProd(service string) string{
	var buildStruct Buildstruct
	fmt.Println("************************************"+service)
	var url ="http://consul-prod.swiggyops.de/v1/kv/config/"+service+"/BUILD_NUMBER"
	//var url ="http://52.76.255.198/v1/kv/config/"+service+"/BUILD_NUMBER"

	fmt.Println("url-------------> :"+url)
	response,err:=http.Get(url)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		jresponse, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(jresponse))
		json.Unmarshal([]byte(jresponse), &buildStruct)
		if (buildStruct != nil) {
			decodedText := getDecodedString(buildStruct[0].Value)
			return decodedText
		}
	}
	return "nothing"
}

func getPodFromService(service string) string {
	var resp map[string]interface{}
	var respop []map[string]interface{}
	fmt.Println("************************************"+service)
	var url ="http://consul.u4.swiggyops.de/v1/health/service/"+service+"?dc=swiggy-perf"
	//	var url ="http://52.76.255.198/v1/kv/config/"+service+"/BUILD_NUMBER"

	fmt.Println("url-------------> :"+url)
	response,err:=http.Get(url)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	}else {
		jresponse, _ := ioutil.ReadAll(response.Body)
		json.Unmarshal([]byte(jresponse), &respop)
		if(len(respop) > 0) {
			resp = respop[0]
			//var s interface{}
			resp = resp["Service"].(map[string]interface{})
			//resp = resp["Tags"].([]interface{})
			tags := resp["Tags"].([]interface{})
			pod := tags[0].(string)
			pod = strings.Replace(pod, "pod=", "", -1)
			if (pod != "") {
				return pod
			}
		}
	}
	return "nothing"

}

func getDecodedString(msg string) string{

	data,err:= base64.StdEncoding.DecodeString(msg)
	if err != nil {
		log.Fatal("error:", err)
	}
	fmt.Printf("%s\n", string(data))
	return 	string(data)
}

func Deploy(service string, buildno string, wg *sync.WaitGroup) {
	if wg != nil {
		defer wg.Done()
	}

	url := "http://deployer.preprod.swiggyops.de/deploy"
	values := map[string]string{"service_name": service, "build_number": buildno, "concurrency_rate": "1", "triggered_by": "auto-scaling"}
	jsonValue, _ := json.Marshal(values)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth("perf","perf@123")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		glog.Error("Error while hitting deploy curl", err)
	}
	glog.Info("Created deploy curl fo service:", service)
	glog.Info("Response is ", resp)
}

func createInfra(service string, buildno string, pod string, ami string, env string, wg *sync.WaitGroup) {
	if wg != nil {
		defer wg.Done()
	}

	url := "http://dragonstone.u4.swiggyops.de/oathkeeper/v1/create/infra"
	values := map[string]interface{}{"name": service, "instance_type": "t2.medium", "count": 1, "build": buildno, "pod" : pod, "ami": ami}
	valuee := map[string]interface{}{service : values}
	val := map[string]interface{}{pod : valuee}
	value := map[string]interface{}{"service": val, "env":env}
	jsonValue, _ := json.Marshal(value)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonValue))
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth("perf","perf@123")
	client := &http.Client{}
	glog.Info("request body is: ",req)
	resp, err := client.Do(req)
	if err != nil {
		glog.Error("Error while hitting deploy curl", err)
	}
	glog.Info("Created deploy curl fo service:", service)
	glog.Info("Response is ", resp.Body)
}

func main() {
	batch := 5
	var wg sync.WaitGroup
	//"ab-framework","ads-consumers","ads-crons","ads-platform","ads-server","agent-service","alchemist","alfred-indexer-api","alfred-indexer-consumers","alfred-indexer-cron","alfred-search-service","alfred-suggest-service","assignment-service","assignment-service-metrics","authy","auto-assign","auto-assign-consumer","availability-read-service","availability-service","banner","base-oms","brandi","cancellation-service","carousel-crud-service","carousel-dashboard","carousel-service","cash-management-service","cconeview-services","cerebro","cerebro-data","chat-middleware-events-handler","chat-middleware-web","checkout-consumer","checkout-little-finger-service","checkout-paas-service","checkout-pricing-service","checkout-service","checkout-track-service","cloud-menu-api","cloud-menu-app","cluster-data","cluster-service","cms-base-service","cms-catalog-ingestion-workflow","cms-category-service","cms-pricing-wrapper","cms-product-service","cms-sku-service","cms-transformation-service","collections-service","collector","collector-auto","conan","consilium",
	services := []string{"context-service","coupon","crm-nps","crm-nps-reconciler","crm-nps-service","crm-track-service","crm-tracking-screen","dash-decorator","dash-delivery-service","dash-fulfilment","dash-order-audit-service","dash-order-intervention","dash-picker-service","dash-pickers","dash-rating-service","dash-serviceability","dash-timeline-service","dash-view-service","de-geofencing","de-location-service","de-nudgeservice","de-onboarding-service","de-tracking-service","delivery-decom","delivery-deshift-upload","delivery-eta-service","delivery-misc","delivery-reports","delivery-service","delivery-service-consumers","delivery-ticketing-system","distance-service","dp-query-interface","drogher","drogon-kafka-avail","drogon-kafka-snd","enigma","entity-service-layer","esme","ext-cart-service","ff-eta-actor","ff-live-orders","ff-messaging-service","ff-service","ff-verification","finance-calcy-service","finance-cash-service","finance-conaro","finance-job-service","finance-nodal-service","finance-recon-platform","finance-reconciliation-service","flashback","flo-executor","flo-metric-collector","flo-server","food-delivery-service","fraud-detection-service","fulfillment-staging","gandalf","gandalf-auto","grasshopper","gringotts","growth-tip","heimdall","helpcenter","hermes","igcc-recovery-service","interocitor","kitchen-fsm","kms-apis","manager-view-service","maps-consumers","marketing-dashboard","meal-service-app","menu-service","mockdataservice","moley","moneta","oms-checkout-service","omt-backend","omt-middleware","oneview","order-edit-service","order-history-service","payment-presentation-service","payout-service","pitara","placing-fsm-service","polygon-dashboard","portal-dweb","portal-dweb-uat","portal-helpcenter","portal-mweb","portal-mweb-uat","post-order-services","prediction-engine","prep-time-service","promise-engine-service","rasmalai","reminder-service","resolutions-service","resolutions-service-test","restaurant-closure-cancellations","restaurant-one-portal","rms","rmsqueueworker","sand-app-config-service","sand-controller","sand-gideon","sand-meals-dashboard","sand-samay","sand-scube","sand-scube-autosuggest","sand-search-controller","sand-user-service","segmentation","self-serve-agent-dashboard","self-serve-service","serviceability-fallback","session-service","shaktimaan","smart-catalog","stress-engine","stress-service","super-app-delivery-service","supply-chain-management","svcplatform-rule","swiggy-inventory-service","swiggy-listing-service","swiggy-menu-diff","swiggy-pricing-service","swiggy-restaurant-services","swiggy-tagging-service","swiggy-taxonomy-service","telephony","tets-sagar","tms","tms-reporting","trade-discount","trip-manager","trip-manager-read-api","varstore","vendor-helpcenter","vendor-insights","vendor_helpcenter","vvo-placer-service","vymo-adapter","watchdog"}
	for count,service := range services {
		wg.Add(1)
		if service != "" {
			build_no := getBuildNumberFromProd(service)
			pod := getPodFromService(service)
			if(!strings.EqualFold(build_no, "nothing") || !strings.EqualFold(pod, "nothing")) {
				//go createInfra(service, build_no, pod, "uat-java-deb-new", "u4", &wg)
				go Deploy(service, build_no, &wg)
				fmt.Println("service is: ", service, pod, build_no)
			}
		}
		if(count %batch == 0 && count > 0) {
			time.Sleep(2 * time.Minute)
		}
	}
	wg.Wait()

}

