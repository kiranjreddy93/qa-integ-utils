package main

import (
	//	"log"
	"fmt"
	"time"
	"github.com/Shopify/sarama"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
	"github.com/go-redis/redis"
	"strings"
)


var (
	kafkaConn = []string{"central-kafka-cluster-1.swiggyperf.in:9092","central-kafka-cluster-2.swiggyperf.in:9092","central-kafka-cluster-3.swiggyperf.in:9092","central-kafka-cluster-4.swiggyperf.in:9092","central-kafka-cluster-5.swiggyperf.in:9092","central-kafka-cluster-6.swiggyperf.in:9092"}
	//kafkaConn = []string{"10.10.24.211:9092","10.10.23.181:9092","10.10.24.121:9092","10.10.21.185:9092","10.10.17.215:9092","10.10.24.1:9092"}
	mytopic = "order_status_update_v2"
	brokerList = kingpin.Flag("brokerList", "List of brokers to connect").Default(kafkaConn...).Strings()
	topic      = kingpin.Flag("topic", "aaaaa").Default("order_status_update_v2").String()
	maxRetry   = kingpin.Flag("maxRetry", "Retry limit").Default("5").Int()
)

func main() {

	kingpin.Parse()
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = *maxRetry
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(*brokerList, config)
	if err != nil {
		fmt.Printf("aaaaaaaa================")
		panic(err)
	}
	defer func() {
		if err := producer.Close(); err != nil {
			panic(err)
		}
	}()
	redisdb := redis.NewClient(&redis.Options{
		Addr:     "perf-test-data-0.redis.swiggyperf.in:6379",
		Password: "", // no password set
		DB:       8,  // use default DB
	})

	pong, err := redisdb.Ping().Result()
	fmt.Println("redis connection",pong, err)
	delivery_boys := []string{"130","635","1343","4076","1055","1","143","1215","624","920","917","154","858","649","1539","908","129","1339","980","319","231","903","831","1981","4954","1317","3477","2875","1237","1363","6347","1180","3345","6871","3318","4862","3648","3326","6013","196","3829","270","923","1282","1310","1980","1355","983","794","3337","534","391","279","629","1073","748","495","1009","510","4596","803","7029","288","1298","5278","364","300","271","1142","1103","5560","5591","2063","5154","1248","651","1552","4897","1333","1286","1496","1444","377","1742","273","1155","743","4721","829","246","1290","1007","327","550","3687","1270","667","1093","6853","5049","1568","663","378","251","1525","6079","4576","1137","2078","1611","355","1168","865","7","175","10","11","1311","1576","1148","322","1005","1261","1116","701","2112","1360","1149","407","393","5744","252","5457","1273","5632","1264","1132","1169","774","387","658","2047","186","502","450","487","1056","6385","1372","548","806","2447","869","478","1434","519","642","1630","647","4749","6763","5042","5054","1200","4046","1341","5616","1515","812","825","632","857","491","872","549","3432","1075","489","581","546","776","2055","6876","1315","1124","2349","533","528","335","744","1289","379","1465","497","4912","6297","2385","6375","6472","1524","4913","1424","1495","1198","764","567","1600","482","1192","1156","1318","1000","6514","1814","683","511","4849","4040","870","762","641","912","476","1691","580","6388","140","689","1641","2844","1346","1347","1869","822","728","950","1864","648","849","606","922","3380","6050","1268","1328","185","2358","5717","384","504","1532","680","672","1927","194","854","561","1272","3275","1139","6790","5594","1788","896","984","157","715","591","1534","705","810","4845","506","578","843","13","14","697","12","420","1562","703","2836","440","7016","6125","6391","3709","2501","4788","724","5377","317","1144","1160","582","3505","156","494","1555","900","1136","786","4846","4836","972","6077","2830","2758","493","702","1421","729","626","747","727","526","627","333","4997","622","726","1511","4861","5206","3693","860","183","6867","305","1914","247","773","2021","310","304","1083","662","1705","736","6749","4882","4729","6379","1426","6360","835","1590","1079","1012","784","166","538","1107","677","643","1408","1415","499","4728","2795","1481","1711","1243","109","371","3980","1335","793","834","1134","556","946","924","768","700","986","599","710","1528","301","1267","1293","734","711","5175","1235","2869","3847","176","208","1510","597","978","6056","615","537","795","873","1985","1549","162","4867","709","278","6063","213","3499","1627","4069","1089","3351","598","2901","5552","5401","3568","469","1566","4454","737","609","646","4049","2035","706","2008","1514","1233","859","6847","3367","1940","739","1921","2859","608","2012","616","3708","3397","1176","340","730","531","719","261","448","5202","1565","1865","1126","732","1217","114","2446","1480","6390","1721","2692","721","1014","1013","1239","15","1227","4730","5723","898","5472","1365","2838","6329","1383","1593","1104","1789","1010","790","1210","484","740","851","4736","838","1922","306","1885","1244","1325","579","761","256","1204","138","4691","1361","6112","1659","2005","1476","4812","4972","5749","1006","899","1234","977","1556","1436","2704","500","819","931","4895","595","992","2025","4927","226","1058","807","959","1545","788","1418","6229","5864","7028","4886","5201","516","4914","827","1526","272","930","564","358","963","161","1755","1586","1660","1541","1509","613","695","968","1074","974","1253","746","5391","1245","1174","6395","1490","690","1020","586","1695","2088","545","6884","4994","752","1236","1763","971","878","965","3375","1745","1031","696","1049","1025","820","6882","346","989","2009","4983","6076","233","769","1220","934","5482","1066","1651","792","1059","1527","1063","2147","918","1030","919","1023","1019","1044","1151","3493","907","3932","1094","1567","669","7002","5596","2075","4941","1456","575","5375","997","1744","1050","1054","292","1086","1084","5246","1106","1558","939","1259","1669","905","1637","1047","1241","1672","1438","3249","1746","949","592","4245","408","1224","888","3467","845","6887","5097","5721","612","2114","1131","619","195","2060","467","823","6407","4957","6344","6408","6078","1666","1353","3413","699","2370","1474","866","1736","5136","1506","717","661","1196","1175","1048","633","2275","596","996","1152","1543","1178","276","1011","1221","1247","1553","1212","1326","1312","221","4999","6058","6086","891","293","6071","1494","1356","1529","1994","5613","5867","2555","1171","4060","6815","4147","1238","5403","1099","2371","1653","1070","4879","5839","5258","2079","2998","1193","6184","2046","1330","294","1704","1670","1561","1352","1448","1008","4101","1535","1428","1449","1663","1680","714","1376","320","1583","5481","152","314","1077","938","1722","2868","4461","264","6797","2322","1085","6172","1853","3028","1982","1559","4998","1016","406","367","5876","716","2061","6012","1167","6332","386","1582","1536","1387","3630","373","1338","1278","998","1269","659","1345","731","1584","1617","4893","1400","3510","1917","970","753","2556","1242","1342","1776","2548","1716","1130","1303","5226","1784","1437","1818","6327","1493","1232","1916","1218","5058","3733","1435","1172","684","1990","2016","4890","1824","6397","954","676","5055","636","3429","2070","6082","522","1829","4955","707","742","1564","1190","7032","7033","1162","1900","1656","2131","1714","1228","1816","1748","1734","461","3502","1777","2049","1713","691","1004","837","2373","2123","1301","1833","2252","2138","1472","2109","1392","544","4420","524","886","1466","1492","1432","2085","1911","1150","5045","1638","3410","767","4432","5172","1464","5589","5742","6065","841","2581","693","558","5598","6376","1479","944","1473","2710","1184","2699","1015","1726","5747","1443","1544","1693","1440","236","2003","1451","2115","5748","1644","1280","232","259","1938","4929","1505","286","1001","1658","1297","1186","868","3984","692","1926","1872","4310","1462","5068","5719","4874","151","4771","5114","6905","4875","219","4902","1839","2228","2865","2255","2018","4023","4512","6217","2558","780","1851","192","1043","1478","1209","1988","735","861","816","3150","1632","4863","2449","1863","1866","133","1537","1974","483","909","994","681","673","220","490","1265","964","1964","951","1057","2033","205","1427","6072","527","4976","1240","6168","4892","5232","911","5497","6907","3999","5483","3435","1161","562","5404","1808","1791","4961","5389","6890","1027","1913","1442","93"}
	//fmt.Println("redis lpop",redisdb.LPop("tst_key"))
	start := time.Now()
	//for {
	for i :=1;i<=8000;i++ {
		order_id := redisdb.LPop("tst_key").Val()
		//fmt.Println("order id is:" ,order_id)
		if order_id != "" {
			assigned := "{ \"order_id\": \"${order_id}\", \"restaurant_id\": 28561, \"restaurant_name\": \"Goli Vada Pav No. 1\", \"rain_mode\": 0, \"banner_factor\": 0, \"batch_id\": 325500575, \"status\": \"assigned\", \"source\": \"app\", \"partner_name\": \"Swiggy\", \"time\": \"2019-03-20 22:25:40\", \"deDetails\": { \"id\": ${id}, \"name\": \"Lakshmanan\", \"nick\": \"Lakshmanan_nickname\", \"mobileNumber\": \"Bb8QSy9zTFQdznPtUVjun3pNMng==\", \"altMobile\": \"Bb8QSpfd4XEZTw3zjwzTxpTpgrg==\", \"lat\": \"12.9328117\", \"lng\": \"77.6029522\", \"de_zone_id\": 2 }, \"deliveryboy_id\": ${id}, \"_de_order_restaurant_bill\": 610, \"_de_order_incoming\": 0, \"params\": { \"bill\": 610, \"collect\": 0 }, \"batch_count\": 1, \"rain_mode_type\": 0, \"is_pop_order\": false, \"geo_details\": { \"restaurant_location\": { \"latAsDouble\": 12.9118083, \"lngAsDouble\": 77.65195700000004, \"lat\": \"12.9118083\", \"lng\": \"77.65195700000004\" }, \"customer_location\": { \"latAsDouble\": 13.045492, \"lngAsDouble\": 80.235645, \"lat\": \"13.045492\", \"lng\": \"80.235645\" }, \"city_id\": \"1\", \"area_id\": \"3\", \"area_name\": \"HSR\", \"zone_id\": \"3\" }, \"finance_details\": { \"system_pay\": 0, \"system_bill\": 199, \"system_collect\": 219, \"de_bill\": 610, \"de_collect\": 0 }, \"geo_fence_breached\": false }"
			assigned = strings.Replace(assigned, "${order_id}", order_id, -1)
			assigned = strings.Replace(assigned, "${id}", delivery_boys[i%len(delivery_boys)], -1)


			confirmed := "{ \"order_id\": \"${order_id}\", \"restaurant_id\": 28561, \"restaurant_name\": \"Goli Vada Pav No. 1\", \"rain_mode\": 0, \"banner_factor\": 0, \"batch_id\": 325500575, \"status\": \"confirmed\", \"source\": \"app\", \"partner_name\": \"Swiggy\", \"time\": \"2019-03-20 22:25:40\", \"deDetails\": { \"id\": ${id}, \"name\": \"Lakshmanan\", \"nick\": \"Lakshmanan_nickname\", \"mobileNumber\": \"Bb8QSy9zTFQdznPtUVjun3pNMng==\", \"altMobile\": \"Bb8QSpfd4XEZTw3zjwzTxpTpgrg==\", \"lat\": \"12.9328117\", \"lng\": \"77.6029522\", \"de_zone_id\": 2 }, \"deliveryboy_id\": ${id}, \"_de_order_restaurant_bill\": 610, \"_de_order_incoming\": 0, \"params\": { \"bill\": 610, \"collect\": 0 }, \"batch_count\": 1, \"rain_mode_type\": 0, \"is_pop_order\": false, \"geo_details\": { \"restaurant_location\": { \"latAsDouble\": 12.9118083, \"lngAsDouble\": 77.65195700000004, \"lat\": \"12.9118083\", \"lng\": \"77.65195700000004\" }, \"customer_location\": { \"latAsDouble\": 13.045492, \"lngAsDouble\": 80.235645, \"lat\": \"13.045492\", \"lng\": \"80.235645\" }, \"city_id\": \"1\", \"area_id\": \"3\", \"area_name\": \"HSR\", \"zone_id\": \"3\" }, \"finance_details\": { \"system_pay\": 0, \"system_bill\": 199, \"system_collect\": 219, \"de_bill\": 610, \"de_collect\": 0 }, \"geo_fence_breached\": false }"
			confirmed = strings.Replace(confirmed,  "${order_id}", order_id, -1)
			confirmed = strings.Replace(confirmed, "${id}", delivery_boys[i%len(delivery_boys)], -1)


			arrived := "{ \"order_id\": \"${order_id}\", \"restaurant_id\": 28561, \"restaurant_name\": \"Goli Vada Pav No. 1\", \"rain_mode\": 0, \"banner_factor\": 0, \"batch_id\": 325500575, \"status\": \"arrived\", \"source\": \"app\", \"partner_name\": \"Swiggy\", \"time\": \"2019-03-20 22:25:40\", \"deDetails\": { \"id\": ${id}, \"name\": \"Lakshmanan\", \"nick\": \"Lakshmanan_nickname\", \"mobileNumber\": \"Bb8QSy9zTFQdznPtUVjun3pNMng==\", \"altMobile\": \"Bb8QSpfd4XEZTw3zjwzTxpTpgrg==\", \"lat\": \"12.9328117\", \"lng\": \"77.6029522\", \"de_zone_id\": 2 }, \"deliveryboy_id\": ${id}, \"_de_order_restaurant_bill\": 610, \"_de_order_incoming\": 0, \"params\": { \"bill\": 610, \"collect\": 0 }, \"batch_count\": 1, \"rain_mode_type\": 0, \"is_pop_order\": false, \"geo_details\": { \"restaurant_location\": { \"latAsDouble\": 12.9118083, \"lngAsDouble\": 77.65195700000004, \"lat\": \"12.9118083\", \"lng\": \"77.65195700000004\" }, \"customer_location\": { \"latAsDouble\": 13.045492, \"lngAsDouble\": 80.235645, \"lat\": \"13.045492\", \"lng\": \"80.235645\" }, \"city_id\": \"1\", \"area_id\": \"3\", \"area_name\": \"HSR\", \"zone_id\": \"3\" }, \"finance_details\": { \"system_pay\": 0, \"system_bill\": 199, \"system_collect\": 219, \"de_bill\": 610, \"de_collect\": 0 }, \"geo_fence_breached\": false }"
			arrived = strings.Replace(arrived,  "${order_id}", order_id, -1)
			arrived = strings.Replace(arrived, "${id}", delivery_boys[i%len(delivery_boys)], -1)


			pickedup := "{ \"order_id\": \"${order_id}\", \"restaurant_id\": 28561, \"restaurant_name\": \"Goli Vada Pav No. 1\", \"rain_mode\": 0, \"banner_factor\": 0, \"batch_id\": 325500575, \"status\": \"pickedup\", \"source\": \"app\", \"partner_name\": \"Swiggy\", \"time\": \"2019-03-20 22:25:40\", \"deDetails\": { \"id\": ${id}, \"name\": \"Lakshmanan\", \"nick\": \"Lakshmanan_nickname\", \"mobileNumber\": \"Bb8QSy9zTFQdznPtUVjun3pNMng==\", \"altMobile\": \"Bb8QSpfd4XEZTw3zjwzTxpTpgrg==\", \"lat\": \"12.9328117\", \"lng\": \"77.6029522\", \"de_zone_id\": 2 }, \"deliveryboy_id\": ${id}, \"_de_order_restaurant_bill\": 610, \"_de_order_incoming\": 0, \"params\": { \"bill\": 610, \"collect\": 0 }, \"batch_count\": 1, \"rain_mode_type\": 0, \"is_pop_order\": false, \"geo_details\": { \"restaurant_location\": { \"latAsDouble\": 12.9118083, \"lngAsDouble\": 77.65195700000004, \"lat\": \"12.9118083\", \"lng\": \"77.65195700000004\" }, \"customer_location\": { \"latAsDouble\": 13.045492, \"lngAsDouble\": 80.235645, \"lat\": \"13.045492\", \"lng\": \"80.235645\" }, \"city_id\": \"1\", \"area_id\": \"3\", \"area_name\": \"HSR\", \"zone_id\": \"3\" }, \"finance_details\": { \"system_pay\": 0, \"system_bill\": 199, \"system_collect\": 219, \"de_bill\": 610, \"de_collect\": 0 }, \"geo_fence_breached\": false }"
			pickedup = strings.Replace(pickedup,  "${order_id}", order_id, -1)
			pickedup = strings.Replace(pickedup, "${id}", delivery_boys[i%len(delivery_boys)], -1)


			reached := "{ \"order_id\": \"${order_id}\", \"restaurant_id\": 28561, \"restaurant_name\": \"Goli Vada Pav No. 1\", \"rain_mode\": 0, \"banner_factor\": 0, \"batch_id\": 325500575, \"status\": \"reached\", \"source\": \"app\", \"partner_name\": \"Swiggy\", \"time\": \"2019-03-20 22:25:40\", \"deDetails\": { \"id\": ${id}, \"name\": \"Lakshmanan\", \"nick\": \"Lakshmanan_nickname\", \"mobileNumber\": \"Bb8QSy9zTFQdznPtUVjun3pNMng==\", \"altMobile\": \"Bb8QSpfd4XEZTw3zjwzTxpTpgrg==\", \"lat\": \"12.9328117\", \"lng\": \"77.6029522\", \"de_zone_id\": 2 }, \"deliveryboy_id\": ${id}, \"_de_order_restaurant_bill\": 610, \"_de_order_incoming\": 0, \"params\": { \"bill\": 610, \"collect\": 0 }, \"batch_count\": 1, \"rain_mode_type\": 0, \"is_pop_order\": false, \"geo_details\": { \"restaurant_location\": { \"latAsDouble\": 12.9118083, \"lngAsDouble\": 77.65195700000004, \"lat\": \"12.9118083\", \"lng\": \"77.65195700000004\" }, \"customer_location\": { \"latAsDouble\": 13.045492, \"lngAsDouble\": 80.235645, \"lat\": \"13.045492\", \"lng\": \"80.235645\" }, \"city_id\": \"1\", \"area_id\": \"3\", \"area_name\": \"HSR\", \"zone_id\": \"3\" }, \"finance_details\": { \"system_pay\": 0, \"system_bill\": 199, \"system_collect\": 219, \"de_bill\": 610, \"de_collect\": 0 }, \"geo_fence_breached\": false }"
			reached = strings.Replace(reached,  "${order_id}", order_id, -1)
			reached = strings.Replace(reached, "${id}", delivery_boys[i%len(delivery_boys)], -1)


			delivered := "{ \"order_id\": \"${order_id}\", \"restaurant_id\": 28561, \"restaurant_name\": \"Goli Vada Pav No. 1\", \"rain_mode\": 0, \"banner_factor\": 0, \"batch_id\": 325500575, \"status\": \"delivered\", \"source\": \"app\", \"partner_name\": \"Swiggy\", \"time\": \"2019-03-20 22:25:40\", \"deDetails\": { \"id\": ${id}, \"name\": \"Lakshmanan\", \"nick\": \"Lakshmanan_nickname\", \"mobileNumber\": \"Bb8QSy9zTFQdznPtUVjun3pNMng==\", \"altMobile\": \"Bb8QSpfd4XEZTw3zjwzTxpTpgrg==\", \"lat\": \"12.9328117\", \"lng\": \"77.6029522\", \"de_zone_id\": 2 }, \"deliveryboy_id\": ${id}, \"_de_order_restaurant_bill\": 610, \"_de_order_incoming\": 0, \"params\": { \"bill\": 610, \"collect\": 0 }, \"batch_count\": 1, \"rain_mode_type\": 0, \"is_pop_order\": false, \"geo_details\": { \"restaurant_location\": { \"latAsDouble\": 12.9118083, \"lngAsDouble\": 77.65195700000004, \"lat\": \"12.9118083\", \"lng\": \"77.65195700000004\" }, \"customer_location\": { \"latAsDouble\": 13.045492, \"lngAsDouble\": 80.235645, \"lat\": \"13.045492\", \"lng\": \"80.235645\" }, \"city_id\": \"1\", \"area_id\": \"3\", \"area_name\": \"HSR\", \"zone_id\": \"3\" }, \"finance_details\": { \"system_pay\": 0, \"system_bill\": 199, \"system_collect\": 219, \"de_bill\": 610, \"de_collect\": 0 }, \"geo_fence_breached\": false }"
			delivered = strings.Replace(delivered,  "${order_id}", order_id, -1)
			delivered = strings.Replace(delivered, "${id}", delivery_boys[i%len(delivery_boys)], -1)


			msg := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(assigned),
			}
			partition, offset, err := producer.SendMessage(msg)
			//producer.SendMessage(msg)
			//start = time.Now()
			//elapsed := time.Since(start)
			//fmt.Println("elapsed time is %s", elapsed)
			time.Sleep(10*time.Millisecond)

			msgc := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(confirmed),
			}
			producer.SendMessage(msgc)
			time.Sleep(10*time.Millisecond)

			msga := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(arrived),
			}
			producer.SendMessage(msga)
			time.Sleep(10*time.Millisecond)

			msgp := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(pickedup),
			}
			producer.SendMessage(msgp)
			time.Sleep(10*time.Millisecond)

			msgr := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(reached),
			}
			producer.SendMessage(msgr)
			time.Sleep(10*time.Millisecond)

			msgd := &sarama.ProducerMessage{
				Topic: *topic,
				Value: sarama.StringEncoder(delivered),
			}
			producer.SendMessage(msgd)
			time.Sleep(10*time.Millisecond)

			if err != nil {
				fmt.Printf("some error occured")
				//panic(err)
			}
			//elapsed := time.Since(start)
			//fmt.Println("elapsed time is %s", elapsed)
			//fmt.Println("assigned valie is",assigned)
			fmt.Printf( " :: Message is stored in topic(%s)/partition(%d)/offset(%d)\n", *topic, partition, offset)
			time.Sleep(1 * time.Microsecond)
		}
	}
	//}
	elapsed := time.Since(start)
	fmt.Printf("elapsed time is %s", elapsed)
}