resource "aws_instance" "ec2_cluster-solr-listing" {
  ami                           = "ami-0d078d0166ea8c775"
  instance_type                 = "t2.medium"
  count                         = "1"
  subnet_id                     = "${module.subnet_public_a.subnet_id_out}"
  vpc_security_group_ids        = ["${aws_security_group.sg.id}"]
  key_name                      = "${module.global_variables.key_name}"
  //associate_public_ip_address   = "true"
  ​
  # metadata tagging
  tags {
    Name               = "u4-solr-listing"
    pod                = "shared"
    env                = "u4"
  }
}
​
resource "aws_instance" "ec2_cluster-solr-polygon" {
ami                           = "ami-0d697f4d244ad4c78"
instance_type                 = "t2.medium"
count                         = "1"
subnet_id                     = "${module.subnet_public_a.subnet_id_out}"
vpc_security_group_ids        = ["${aws_security_group.sg.id}"]
key_name                      = "${module.global_variables.key_name}"
//associate_public_ip_address   = "true"
​
# metadata tagging
tags {
Name               = "u4-solr-polygon"
pod                = "shared"
env                = "u4"
}
}
​
​
resource "aws_instance" "ec2_cluster-redis" {
ami                           = "ami-0ea0adfef408c831e"
instance_type                 = "t2.medium"
count                         = "12"
subnet_id                     = "${module.subnet_public_a.subnet_id_out}"
vpc_security_group_ids        = ["${aws_security_group.sg.id}"]
key_name                      = "${module.global_variables.key_name}"
associate_public_ip_address   = "false"
user_data = "IyEvdXNyL2Jpbi9lbnYgYmFzaAoKeXVtIC15IHVwZGF0ZQp5dW0gLXkgaW5zdGFsbCBnY2MgbWFrZQpjZCAvdXNyL2xvY2FsL3NyYwp3Z2V0IGh0dHA6Ly9kb3dubG9hZC5yZWRpcy5pby9yZWxlYXNlcy9yZWRpcy01LjAtcmM0LnRhci5negp0YXIgeHpmIHJlZGlzLTUuMC1yYzQudGFyLmd6CmNkIHJlZGlzLTUuMC1yYzQKbWFrZSBkaXN0Y2xlYW4KbWFrZQp5dW0gaW5zdGFsbCAteSB0Y2wKbWFrZSB0ZXN0Cm1rZGlyIC1wIC9ldGMvcmVkaXMgL3Zhci9saWIvcmVkaXMgL3Zhci9yZWRpcy82Mzc5CmNwIHNyYy9yZWRpcy1zZXJ2ZXIgc3JjL3JlZGlzLWNsaSAvdXNyL2xvY2FsL2JpbgpjcCByZWRpcy5jb25mIC9ldGMvcmVkaXMvNjM3OS5jb25mCndnZXQgLU8gL2V0Yy9pbml0LmQvcmVkaXMtc2VydmVyIGh0dHBzOi8vZ2lzdC5naXRodWJ1c2VyY29udGVudC5jb20vbHNiYXJkZWwvMju4Mjk4L3Jhdy9kNDhiODRkODkyODlkZjM5ZWFkZGM1M2YxZTlhOTE4Zjc3NmIzMDc0L3JlZGlzLXNlcnZlci1mb3ItaW5pdC5kLXN0YXJ0dXAKY2htb2QgNzU1IC9ldGMvaW5pdC5kL3JlZGlzLXNlcnZlcgpjaGtjb25maWcgLS1hZGQgcmVkaXMtc2VydmVyCmNoa2NvbmZpZyAtLWxldmVsIDM0NSByZWRpcy1zZXJ2ZXIgb24="
ebs_block_device {
device_name = "/dev/sda1"
volume_size = 100
}
​
# metadata tagging
tags {
Name               = "u4-redis-${count.index}"
pod                = "shared"
env                = "u4"
}
}
​
​
resource "aws_instance" "ec2_cluster-rmq" {
ami                           = "ami-03261c40eea68dab2"
instance_type                 = "t2.medium"
count                         = "1"
subnet_id                     = "${module.subnet_public_a.subnet_id_out}"
vpc_security_group_ids        = ["${aws_security_group.sg.id}"]
key_name                      = "${module.global_variables.key_name}"
associate_public_ip_address   = "false"
ebs_block_device {
device_name = "/dev/sda1"
volume_size = 100
}
​
# metadata tagging
tags {
Name               = "u4-central-rmq"
pod                = "shared"
env                = "u4"
}
}
​
resource "aws_instance" "ec2_cluster-kafka" {
ami = "ami-071601048d114f18f"
instance_type = "t2.medium"
count = "1"
subnet_id                     = "${module.subnet_public_a.subnet_id_out}"
vpc_security_group_ids = ["${aws_security_group.sg.id}"]
key_name = "${module.global_variables.key_name}"
associate_public_ip_address = "false"
ebs_block_device {
device_name = "/dev/sda1"
volume_size = 100
}
​
# metadata tagging
tags {
Name = "u4-central-kafka"
pod = "shared"
env = "u4"
}
}